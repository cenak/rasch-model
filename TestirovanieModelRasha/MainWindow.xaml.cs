﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TestirovanieModelRasha
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    /// 


    public partial class MainWindow : Window
    {

        private int m = 20;    // Ичло людей
        private int n = 20;    // Число задач
        private double[] beta;  // Трудность задачи
        private double[] teta;  // Подготовленность актора
        private double[] betaSum; // 
        private double[] tetaSum; // 
        private double veroyatnost;
        private int[,] R;
        Random rand = new Random();

        public MainWindow()
        {
            R = new int[m,n];
            teta = new double[m];
            beta = new double[n];

            tetaSum = new double[m];
            betaSum = new double[n];

            for (int i = 0; i < m; i++)
                teta[i] = -10 + 20 * rand.NextDouble();

            for (int i = 0; i < n; i++)
                beta[i] = -10 + 20 * rand.NextDouble();

            InitializeComponent();
        }

        void ForwardTask(object sender, EventArgs e)
        {
            //Fill the matrix of tasks solutions
            for (int j = 0; j < m; j++)
            {
                for (int i = 0; i < n; i++)
                {
                    if (rand.NextDouble() > (1 / (1 + Math.Exp(beta[i] - teta[j]))))
                        R[j,i] = 1;
                    else
                        R[j,i] = 0;
                }
            }

            for (int i = 0; i < m; i++)
                tetaSum[i] = 0;

            for (int i = 0; i < n; i++)
                betaSum[i] = 0;

            for (int i = 0; i < m; i++)
                for (int j = 0; j < n; j++)
                    tetaSum[i] += R[i, j];

            for (int i = 0; i < n; i++)
                for (int j = 0; j < m; j++)
                    betaSum[i] += R[j, i];

            Massive.Text = "";

            Massive.Text += "Started matrix\n\n";

            for (int j = 0; j < m; j++)
            {
                for (int i = 0; i < n; i++)
                {
                    Massive.Text += R[j,i].ToString() + " | ";
                }

                Massive.Text += $"   tetaSum = {tetaSum[j]} \n";
            }

            MassiveBackwardTask.Text += "\n";

            for (int i = 0; i < m; i++)
            {
                Massive.Text += $"{betaSum[i]} | ";
            }

            Massive.Text += $"  - betaSum";
        }

        void BackwardTask(object sender, EventArgs e)
        {
            MassiveBackwardTask.Text = "";

            for (int i = 0; i < m; i++)
                tetaSum[i] = 0;

            for (int i = 0; i < n; i++)
                betaSum[i] = 0;

            for (int i = 0; i < m; i++)
                for (int j = 0; j < n; j++)
                    tetaSum[i] += R[i, j];

            for (int i = 0; i < n; i++)
                for (int j = 0; j < m; j++)
                    betaSum[i] += R[j, i];

            double temp;

            int[] tempR = new int[m];

            //Sorting people
            //Maybe this works... I hope...
            for (int i = 0; i < m; i++)
            {
                for (int j = i + 1; j < m; j++)
                {
                    if (tetaSum[i] > tetaSum[j])
                    {
                        temp = tetaSum[i];
                        tetaSum[i] = tetaSum[j];
                        tetaSum[j] = temp;

                        for (int k = 0; k < n; k++)
                        {
                            tempR[k] = R[i, k];
                            R[i, k] = R[j, k];
                            R[j, k] = tempR[k];
                        }
                    }
                }
            }

            //Sorting tasks
            //I hope this works too...
            for (int i = 0; i < n; i++)
            {
                for (int j = i + 1; j < n; j++)
                {
                    if (betaSum[i] > betaSum[j])
                    {
                        temp = betaSum[i];
                        betaSum[i] = betaSum[j];
                        betaSum[j] = temp;

                        for (int k = 0; k < m; k++)
                        {
                            tempR[k] = R[k, i];
                            R[k, i] = R[k, j];
                            R[k, j] = tempR[k];
                        }
                    }
                }
            }

            MassiveBackwardTask.Text += "Triangle matrix \n\n";

            for (int j = 0; j < m; j++)
            {
                for (int i = 0; i < n; i++)
                {
                    MassiveBackwardTask.Text += R[j, i].ToString() + " | ";
                }

                MassiveBackwardTask.Text += $"   tetaSum = {tetaSum[j]} \n";
            }

            MassiveBackwardTask.Text += "\n";

            for (int i = 0; i < m; i++)
            {
                MassiveBackwardTask.Text += $"{betaSum[i]} | ";
            }

            //Chance calculating

            double[,] sR = new double[11, 11];
            //double[,] chanceR = new double[m, n];
            double[,] chanceR = new double[11, 11];

            for (int j = 5, jj = 0; j < m - 4; j++, jj++)
            {
                for (int i = 5, ii = 0; i < n - 4; i++, ii++)
                {
                    chanceR[jj, ii] = GetChance(j, i);
                }
            }

            MassiveBackwardTask.Text += "\n\nChance matrix\n\n";

            for (int j = 0; j < 11; j++)
            {
                for (int i = 0; i < 11; i++)
                {
                    MassiveBackwardTask.Text += Math.Round(chanceR[j, i], 5).ToString() + " | ";
                }
                MassiveBackwardTask.Text += "\n";
            }

            //After this we work with matrix 11x11

            MassiveBackwardTask.Text += "\n\nC[j,i] matrix\n\n";

            double[,] CRji = new double[11, 11];

            for (int j = 0; j < 11; j++)
            {
                for (int i = 0; i < 11; i++)
                {
                    CRji[j, i] = Math.Log((1 - chanceR[j, i]) / chanceR[j, i]);
                }
            }

            for (int j = 0; j < 11; j++)
            {
                for (int i = 0; i < 11; i++)
                {
                    MassiveBackwardTask.Text += Math.Round(CRji[j, i], 5).ToString() + " | ";
                }
                MassiveBackwardTask.Text += "\n";
            }

            double[] tetaBT = new double[11];
            double[] betaBT = new double[11];

            for (int k = 0; k < 10; k++)
            {
                for (int j = 0; j < 11; j++)
                {
                    for (int i = 0; i < 11; i++)
                    {
                        tetaBT[j] += betaBT[i] - CRji[j, i];
                    }
                    tetaBT[j] /= 11;
                }

                for (int j = 0; j < 11; j++)
                {
                    for (int i = 0; i < 11; i++)
                    {
                        betaBT[j] += tetaBT[i] + CRji[i, j];
                    }
                    betaBT[j] /= 11;
                }
            }

            MassiveBackwardTask.Text += "\n\n\n\ntetas: ";

            for (int j = 0; j < 11; j++)
            {
                MassiveBackwardTask.Text += Math.Round(tetaBT[j], 2).ToString() + " | ";
            }

            MassiveBackwardTask.Text += "\n\n\n\nbetas: ";

            for (int j = 0; j < 11; j++)
            {
                MassiveBackwardTask.Text += Math.Round(betaBT[j], 2).ToString() + " | ";
            }

            MassiveBackwardTask.Text += "\n\n\n\n";
        }

        double GetChance(int j, int i)
        {
            double[,] sR = new double[11, 11];
            double chance = 0;

            for (int k = j - 5, KK = 0; k < j + 5; k++, KK++)
            {
                for (int l = i - 5, LL = 0; l < i + 5; l++, LL++)
                {
                    sR[KK, LL] = R[k, l];
                }
            }

            double sum = 0;

            for (int k = 0; k < 11; k++)
            {
                for (int l = 0; l < 11; l++)
                {
                    sum += sR[k,l];
                }
            }

            chance = sum / 121;

            return chance;
        }
    }
}
